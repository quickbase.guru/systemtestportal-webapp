/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package email

import (
	"net/smtp"
	"net/textproto"

	"github.com/jordan-wright/email"
)

// STPEmail wraps the Email type of
// "github.com/jordan-wright/email"
//
// This makes the sending and
// configuration of emails easier.
type STPEmail struct {
	email.Email
}

// New creates a new STPEmail with all the information given.
//
// Returns the email.
func New(to, bcc, cc []string, subject string, text, html []byte) *STPEmail {
	// from is what is being displayed as
	// the sender of the email.
	// Some email-providers might not support
	// sending emails under aliases (e.g. gmail).
	// Then the email-address that is used as
	// authentication is displayed as sender.
	from := "systemtestportal@systemtestportal.com"

	return &STPEmail{
		Email: email.Email{
			From:    from,
			To:      to,
			Bcc:     bcc,
			Cc:      cc,
			Subject: subject,
			Text:    text,
			HTML:    html,
			Headers: textproto.MIMEHeader{},
		},
	}
}

// Send sends the STPEmail.
//
// It uses the configured settings for the mail server.
//
// Returns an error if any occurred.
func (e STPEmail) Send() error {
	auth := GetAuthentication()
	err := e.Email.Send(
		configuration.GetAddress(),
		smtp.PlainAuth("", auth.Username, auth.Password, configuration.Hostname))
	if err != nil {
		return err
	}

	return nil
}

// AttachFile attaches a file with the given name
// to the STPEmail.
//
// Returns an error if any occurred.
func (e STPEmail) AttachFile(filename string) error {
	_, err := e.Email.AttachFile(filename)
	return err
}
