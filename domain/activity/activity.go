/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package activity

import (
	"html/template"
	"log"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

// These are the types of activities
const ASSIGN_CASE int = 0
const ASSIGN_SEQUENCE int = 1
const CREATE_CASE int = 2
const CREATE_SEQUENCE int = 3
const PROTOCOL_CASE int = 4
const PROTOCOL_SEQUENCE int = 5

type ActivityItem interface {
	// Get rendered item template makes the ActivityItem execute it's template on itself and return the resulting string buffer.
	// This way we can have every Item decide how it looks by having its own template.
	RenderItemTemplate(activity *Activity, lang string) (template.HTML, error)
}

type Activity struct {
	// Automated Xorm Fields -----------|
	Id        int64
	CreatedAt time.Time `xorm:"created"`
	// Foreign Keys --------------------|
	ProjectID int64
	AuthorId  string

	ActivityItemId int64
	// ---------------------------------|

	ActivityType int

	Author       *user.User       `xorm:"-"`
	Project      *project.Project `xorm:"-"`
	ActivityItem ActivityItem     `xorm:"-"`
}

type ActivityEntities struct {
	User             *user.User
	Project          *project.Project
	Case             *test.Case
	Sequence         *test.Sequence
	CaseProtocol     *test.CaseExecutionProtocol
	SequenceProtocol *test.SequenceExecutionProtocol
}

func (activity Activity) GetRenderedItemTemplate(lang string) template.HTML {
	executedTemplate, err := activity.ActivityItem.RenderItemTemplate(&activity, lang)

	if err != nil {
		log.Println(err)
		return ""
	}

	return executedTemplate
}
