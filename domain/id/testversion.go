/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package id

import (
	"encoding/json"
	"strconv"
	"strings"
)

// TestVersionExistenceChecker provides everything needed to check, if a testVersion is already existent in storage.
type TestVersionExistenceChecker interface {
	//Exists checks, if the given testVersion is already existent in storage.
	Exists(id TestVersionID) (bool, error)
}

//TestVersionID hold all needed data to identify the a test version in the whole system.
//This is the number of the version and the superior test
//Test versions can be versions of cases or sequences
type TestVersionID struct {
	TestID
	testVersion int
}

//NewTestVersionID returns a new TestVersionID containing the given version number and superior test.
//No validation will be performed.
func NewTestVersionID(test TestID, testVersion int) TestVersionID {
	return TestVersionID{test, testVersion}
}

//TestVersion returns the version number of the test version
func (id TestVersionID) TestVersion() int {
	return id.testVersion
}

//Validate checks if this is a valid id for a new test version.
func (id TestVersionID) Validate(tvec TestVersionExistenceChecker) error {
	if id.TestVersion() <= 0 {
		return nonPositiveVersionNr()
	}
	exists, err := tvec.Exists(id)
	if err != nil {
		return err
	}
	if exists {
		return alreadyExistsErr(id)
	}
	return nil
}

//toStrings is a helper function for encoding. Returns all fields as a string slice
func (id *TestVersionID) toStrings() []string {
	return []string{
		id.Owner(),
		id.Project(),
		id.Test(),
		strconv.FormatBool(id.IsCase()),
		strconv.Itoa(id.TestVersion()),
	}
}

//fromStrings is a helper function for decoding. Fills the fields with the information given in the string slice
func (id *TestVersionID) fromStrings(strings []string) (err error) {
	id.ActorID = ActorID(strings[0])
	id.project = strings[1]
	id.test = strings[2]
	id.isCase, err = strconv.ParseBool(strings[3])
	if err != nil {
		return
	}
	id.testVersion, err = strconv.Atoi(strings[4])
	return

}

//GobEncode encodes the id to a byte stream so it can be stored for example to a session
func (id *TestVersionID) GobEncode() ([]byte, error) {
	idString := strings.Join(id.toStrings(), "?")
	return []byte(idString), nil
}

//GobDecode decodes the id from a byte stream. All fields of the id called on will be overwritten
func (id *TestVersionID) GobDecode(data []byte) (err error) {
	idString := string(data)
	ids := strings.Split(idString, "?")
	return id.fromStrings(ids)
}

//these are the field-names for the json encoding
const (
	ownerKey       = "Owner"
	projectKey     = "Project"
	testKey        = "Test"
	testCaseKey    = "isCase"
	testVersionKey = "TestVersion"
)

//MarshalJSON will marshal the id into json
func (id TestVersionID) MarshalJSON() ([]byte, error) {
	idStrings := id.toStrings()

	ID := make(map[string]string)
	ID[ownerKey] = idStrings[0]
	ID[projectKey] = idStrings[1]
	ID[testKey] = idStrings[2]
	ID[testCaseKey] = idStrings[3]
	ID[testVersionKey] = idStrings[4]

	return json.Marshal(&ID)
}

//UnmarshalJSON will unmarshal the id from a given json. All fields of the id called on will be overwritten
func (id *TestVersionID) UnmarshalJSON(data []byte) (err error) {
	ID := make(map[string]string)
	err = json.Unmarshal(data, &ID)
	if err != nil {
		return
	}
	idStrings := make([]string, 5)
	idStrings[0] = ID[ownerKey]
	idStrings[1] = ID[projectKey]
	idStrings[2] = ID[testKey]
	idStrings[3] = ID[testCaseKey]
	idStrings[4] = ID[testVersionKey]
	err = id.fromStrings(idStrings)
	return
}
