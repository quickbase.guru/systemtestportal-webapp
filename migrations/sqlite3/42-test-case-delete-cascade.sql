-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
ALTER TABLE test_sequence_version_test_cases RENAME TO test_sequence_version_test_cases_old;

CREATE TABLE test_sequence_version_test_cases (
    id INTEGER PRIMARY KEY,
    test_sequence_version INTEGER NOT NULL REFERENCES test_sequence_versions(id) ON DELETE CASCADE,
    test_case INTEGER NOT NULL REFERENCES test_cases(id) ON DELETE CASCADE,
    case_index INTEGER NOT NULL
);

INSERT INTO test_sequence_version_test_cases
 SELECT * FROM test_sequence_version_test_cases_old;

DROP TABLE test_sequence_version_test_cases_old;


-- +migrate Down
ALTER TABLE test_sequence_version_test_cases RENAME TO test_sequence_version_test_cases_old;

CREATE TABLE test_sequence_version_test_cases (
    id INTEGER PRIMARY KEY,
    test_sequence_version INTEGER NOT NULL REFERENCES test_sequence_versions(id) ON DELETE CASCADE,
    test_case INTEGER NOT NULL REFERENCES test_cases(id),
    case_index INTEGER NOT NULL
);

INSERT INTO test_sequence_version_test_cases(test_sequence_version, test_case, case_index)
 SELECT * FROM test_sequence_version_test_cases_old;

DROP TABLE test_sequence_version_test_cases_old;