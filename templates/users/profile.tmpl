{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "content"}}
{{ if not .Profile.IsDeactivated }}
<div class="container">
    <div class="card p-4">
        <div class="row justify-content-center mb-4">
             <div class="col-4 col-sm-2 p-2">
                 <img src="{{getImagePath .Profile.Image "user"}}" class="img-fluid image-responsive rounded profile-picture">
             </div>
             <div class="col-12 col-sm-10 col-md-6">
                 <h3 id="displayName">{{.Profile.DisplayName}}</h3>
                 <p id="username" class="m-0"><strong>{{T "Username" .}}: </strong>{{.Profile.Name}}</p>
                 {{if .Profile.IsEmailPublic}}
                    <p id="email" class="m-0"><strong>{{T "E-Mail" .}}: </strong>{{.Profile.Email}}</p>
                 {{end}}
                 <p id="registrationDate"><strong>{{T "Registered at" .}}: </strong>{{.Profile.RegistrationDate}}</p>
                 <p id="bioHead" class="m-0"><strong>{{T "About me" .}}:</strong></p>
                 <div id="userBio">
                    {{with .Profile.Biography}}
                        {{printMarkdown .}}
                    {{else}}
                        <em>{{T "This user has not added information about themselves yet" .}}!</em>
                    {{end}}
                 </div>
                 {{with .Edit}}
                     <button class="btn btn-primary mr-2 mt-4" id="buttonEdit" formaction="edit" onclick="window.location.href = window.location.href + '/settings'">
                         <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                         <span class="d-none d-md-inline">{{T "Edit" .}}</span>
                     </button>
                 {{end}}
             </div>
        </div>
    </div>
</div>
{{ else }}
<div class="container">
    <div class="row justify-content-center mb-4">
        <img src="/static/img/STP-Logo.png" class="img-fluid image-responsive rounded"
             style="width: 256px; height: 256px;">
    </div>
    <div class="row justify-content-md-center">
        <div class="col-12 col-md-8">
            <p>{{T "This user has been deactivated." .}}</p>
        </div>
    </div>
</div>
{{ end }}
{{end}}


