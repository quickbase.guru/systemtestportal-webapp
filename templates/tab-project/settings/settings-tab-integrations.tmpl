{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

<!-- Translated to German -->
{{define "settings-tab"}}
{{template "modal-generic-error" .}}

<div id="integrations-settings-content">

    <div class="row-md-8 tab-side-bar-row">
        <div class="form-group p-3">
            <h3>
            {{T "Integration in the CI/CD-Pipeline" .}}
            </h3>
            <label for="projectCIUrl"><strong>Project url for CI/CD</strong></label>
            <p>The url of your project for the integration into the CI/CD.
                Copy this and add it as <b>STP_URL</b> to your Environment Variables in your CI/CD.
            </p>
            <div class="form-group">
                <form type="text" class="form-control" id="projectCIUrl" readonly>/{{ .Project.Owner }}/{{ .Project.Name }}/ci</form>
            </div>
            <label for="projectCIToken"><strong>Project token</strong></label>
            <p>Your token for the integration into the CI/CD. Copy this and add it as <b>STP_TOKEN</b> to your Environment Variables in your CI/CD</p>
            <div class="form-group">
                <form type="text" class="form-control" id="projectCIToken" readonly>{{ .Project.Token }}</form>
            </div>
        </div>
        <div class="form-group p-3">
            <h3>
            {{T "Gitlab Integration" .}}
            </h3>
            <p>
                Integrate the SystemTestPortal into your GitLab project to automatically create issues when a test
                fails.
            </p>
            <div class="form-check mb-2">
                <input class="form-check-input" type="checkbox" value=""
                       id="inputIsActive" {{ if eq .Project.Integrations.GitlabProject.IsActive true}} checked {{ end }}>
                <label class="form-check-label" for="inputIsActive">
                    Activate GitLab Integration
                </label>
            </div>

            <div class="mb-2">
                <label for="inputIntegrationGitlabProjectID"><strong>{{T "Project ID" .}}</strong></label>
                <p>The project id (or url-encoded path) of your GitLab project.</p>
                <input class="form-control" id="inputIntegrationGitlabProjectID"
                       value="{{ .Project.Integrations.GitlabProject.ID }}"/>
            </div>
            <div class="mb-2">
                <label for="inputIntegrationGitlabProjectToken"><strong>{{T "Project Token" .}}</strong></label>
                <p>The token to access your project. See <a href="https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html" target="_blank">the GitLab documentation</a> for more information. </p>
                <input class="form-control" id="inputIntegrationGitlabProjectToken"
                       value="{{ .Project.Integrations.GitlabProject.Token }}"/>
            </div>
        </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/project/settings/settings-integrations.js" integrity="{{sha256 "/static/js/project/settings/settings-integrations.js"}}"></script>
<script src="/static/js/util/generate_help.js" integrity="{{sha256 "/static/js/util/generate_help.js"}}"></script>

<script>
    initializeIntegrationsSettingsListener();
    generateHelp($("#settings-nav"));
    $('#projectCIUrl').prepend(document.location.hostname);
</script>
{{end}}