{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}

{{template "modal-delete-confirm" .DeleteTestSequence}}

{{$parent := .}}
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Test Sequence Details" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="3">
                            {{T "This shows the details of a test sequence" .}}.
                        </td>
                    </tr>
                    <tr class="h5">
                        <th>{{T "Buttons" .}}</th>
                        <th>{{T "Function" .}}</th>
                        <th><span class="d-none d-sm-inline">{{T "Shortcut" .}}</span></th>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-secondary">
                                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> Back</span>
                            </button>
                        </td>
                        <td>{{T "Get back to the test sequence list" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>Backspace</button></td>
                    </tr>
                {{ if .ProjectPermissions.DeleteSequence }}
                    <tr>
                        <td>
                            <button class="btn btn-danger">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Delete" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Open a confirm dialog before deleting the current test sequence" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>Del</button></td>
                    </tr>
                {{ end }}
                {{ if .ProjectPermissions.DuplicateSequence }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" formaction="duplicate">
                                <i class="fa fa-clone" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Duplicate" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Duplicate the test sequence with history up to the currently selected version" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>D</button></td>
                    </tr>
                {{ end }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" formaction="history">
                                <i class="fa fa-history" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "History" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Show a list with all previous versions of this test sequence" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>H</button></td>
                    </tr>
                {{ if .ProjectPermissions.EditSequence }}
                    {{ if (ne .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr) }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" formaction="revert">
                                <abbr title="Reverts the test case to the selected version">
                                    <i class="fa fa-undo" aria-hidden="true"></i>
                                    <span class="d-none d-sm-inline">{{T "Revert" .}}</span>
                                </abbr>
                            </button>
                        </td>
                        <td>{{T "Revert the test sequence to the selected version" .}}.</td>
                        <td><span class="d-none d-sm-inline">-</span></td>
                    </tr>
                    {{ else }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" formaction="edit">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Edit" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Edit the current test sequence" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>E</button></td>
                    </tr>
                    {{ end }}
                {{ end }}
                {{ if and .ProjectPermissions.AssignSequence (eq .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr) }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" disabled>
                                <i class="fa fa-user-plus" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Assignees" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Assign this test sequence as a task to a team member" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>A</button></td>
                    </tr>
                {{ end }}
                {{ if and .ProjectPermissions.Execute (eq .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr) }}
                    <tr>
                        <td>
                            <button class="btn btn-success" disabled>
                                <i class="fa fa-play" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Start" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Start the execution of the current test sequence" }}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>{{T "Enter" .}}</button></td>
                    </tr>
                {{ end }}
                </table>
                <span class="mt-3 float-left">{{T "For more information visit our" .}} <a href="http://docs.systemtestportal.org" target="_blank">{{T "documentation" .}}</a>.</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="revertModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Revert Test Sequence" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>{{T "Do you really want to revert the test sequence to this version" .}}?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="buttonRevertConfirmed">{{T "Revert" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="duplicateModal">
    <script>
        var sequences = {{ .TestSequences }};
    </script>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fa fa-clone"></i> {{T "Duplicate Test Sequence" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>{{T "This duplicates the current test sequence up to the currently shown version" .}}.
                {{T "The test sequence duplicate needs a unique name" .}}.</p>
                <form id="duplicateTestSequenceForm">
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <input class="form-control" id="inputDuplicateTestSequenceName" name="inputDuplicateTestSequenceName"
                                   placeholder="Enter name for test sequence duplicate">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="buttonDuplicateConfirmed" disabled>{{T "Duplicate" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>
{{ template "modal-tester-assignment" . }}
<div class="tab-card card" id="tabTestsequences">
    <nav class="navbar navbar-light action-bar p-3 d-print-none">
        <div class="input-group flex-nowrap">
            <button class="btn btn-secondary mr-2" id="buttonBack">
                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                <span class="d-none d-lg-inline"> {{T "Back" .}}</span>
            </button>
        {{ if .ProjectPermissions.DeleteSequence }}
            <button class="btn btn-danger mr-2" id="buttonDelete" data-toggle="modal"
                    data-target="#deleteTestSequence">
                <i class="fa fa-trash-o" aria-hidden="true"></i>
                <span class="d-none d-lg-inline"> {{T "Delete" .}}</span>
            </button>
        {{ end }}
        {{ if .ProjectPermissions.EditSequence }}
            <button class="btn btn-primary mr-2" data-toggle="modal"
                    id="buttonDuplicate" formaction="duplicate" data-target="#duplicateModal">
                <i class="fa fa-clone" aria-hidden="true"></i>
                <span class="d-none d-md-inline"> {{T "Duplicate" .}}</span>
            </button>
        {{ end }}
            <button class="btn btn-primary mr-2" id="buttonHistory" formaction="history">
                <i class="fa fa-history" aria-hidden="true"></i>
                <span class="d-none d-md-inline"> {{T "History" .}}</span>
            </button>
        {{ if .ProjectPermissions.EditSequence }}
        {{ if (ne .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr) }}
            <button class="btn btn-primary mr-2" formaction="revert" data-toggle="modal"
                    data-target="#revertModal">
                <abbr title="Reverts the test case to the selected version">
                    <i class="fa fa-undo" aria-hidden="true"></i>
                    <span class="d-none d-md-inline">{{T "Revert" .}}</span>
                </abbr>
            </button>
        {{ else }}
            <button class="btn btn-primary mr-2" id="buttonEdit" formaction="edit">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                <span class="d-none d-md-inline"> {{T "Edit" .}}</span>
            </button>
        {{ end }}
        {{ end }}
        {{ if and .ProjectPermissions.AssignSequence (eq .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr) }}
            <button class="btn btn-primary mr-2 float-right" id="buttonAssign" data-toggle="modal"
                    data-target="#modal-tester-assignment">
                <i class="fa fa-user-plus" aria-hidden="true"></i>
                <span class="d-none d-md-inline"> {{T "Assignees" .}}</span>
            </button>
        {{ end }}
        {{ if and .ProjectPermissions.Execute (eq .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr)}}
        {{ with .TestSequenceVersion }}
        {{ if and .Cases .SequenceInfo.Versions}}
        <button class="btn btn-success" id="buttonExecute">
        {{ else if .SequenceInfo.Versions}}
        <button class="btn btn-success" id="buttonExecute" disabled data-toggle="tooltip" data-placement="bottom"
                title="" data-original-title="{{T "This test sequence has no cases" .}}!">
        {{ else }}
        <button class="btn btn-success" id="buttonExecute" disabled data-toggle="tooltip" data-placement="bottom"
                title="" data-original-title="{{T "This Test Sequence is not applicable to any system-under-test-versions" .}}!">
        {{ end }}
            <i class="fa fa-play" aria-hidden="true"></i>
            <span class="d-none d-md-inline"> {{T "Start" .}}</span>
        </button>
        {{ end }}
        {{ end }}
        </div>
    </nav>
    <div class="row tab-side-bar-row">
    <div class="col-md-9 p-3">
    {{ if ne .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr }}
        <div class="alert alert-warning alert-dismissible">
            <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>{{T "Warning" .}}!</strong> {{T "You are viewing an older version of the test sequence" .}}
        </div>
    {{ end }}
        <h4 class="mb-3">{{ .TestSequence.Name }}</h4>
    {{ with .TestSequenceVersion }}
        <div class="form-group">
            <label><strong>{{T "Test Sequence Description" .}}</strong></label>
            <div class="text-muted">
            {{ with .Description }}
                        {{printMarkdown . }}
                    {{ else }}
                {{T "No Description" .}}
            {{ end }}
            </div>
        </div>
        <div class="form-group">
            <label><strong>{{T "Test Sequence Preconditions" .}}</strong></label>
            <ul class="list-group" id="preconditionsList">
            {{ if .Preconditions }}
            {{ range .Preconditions }}
                <li class="list-group-item preconditionItem">
                    <span>{{ .Content }}</span>
                </li>
            {{ end }}
            {{ else }}
                <li class="list-group-item">
                    <span>{{T "No preconditions." .}}</span>
                </li>
            {{ end }}
            </ul>
        </div>
        <div class="form-group">
            <label><strong>{{T "Test Cases" .}}</strong></label>
        {{ if .Cases }}
            <ul class="list-group">
            {{ range .Cases }}
                <li class="list-group-item testCaseLine" id="{{ .Name }}">
                {{ .Name }}
                </li>
            {{ end }}
            </ul>
        {{ else }}
            <p class="text-muted">{{T "No Test Cases" .}}</p>
        {{ end }}
        </div>
    {{template "comments" $parent}}
    </div>
    <div class="col-md-3 p-3 tab-side-bar">
        <div class="form-group">
            <label><strong>{{T "Applicability" .}}</strong></label>
        {{ if .SequenceInfo.Versions }}
            <div class="form-group">
                <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                    <label for="inputTestCaseSUTVersions">
                        <strong>{{T "Versions" .}}</strong>
                    </label>
                </div>
                <div class="col-10 col-sm-11 col-md-12 col-lg-12">
                    <select class="custom-select mb-2" id="inputTestCaseSUTVersions">

                    </select>
                </div>
                <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                    <label for="inputTestCaseSUTVariants">
                        <strong>{{T "Variants" .}}</strong>
                    </label>
                </div>
                <div class="form-group col-10 col-sm-11 col-md-12 col-lg-12">
                    <ul class="list-group" id="inputTestCaseSUTVariants">
                    </ul>
                </div>
            </div>
        {{ else }}
            <div class="form-group">
                <p class="text-muted">{{T "This Test Sequence is not applicable to any system-under-test-versions" .}}.
                    {{T "Edit the Test Sequence and select applicable versions" .}}.</p>
            </div>
        {{ end }}
        </div>
        <div class="form-group">
            <label><strong>{{T "Estimated Test Duration" .}}</strong></label>
            <p class="text-muted">
            {{ if and (eq .SequenceInfo.DurationHours 0) (eq .SequenceInfo.DurationMin 0) }}
                No Test Duration
            {{ else }}
            {{ if gt .SequenceInfo.DurationHours 0 }}
            {{ .SequenceInfo.DurationHours }}
            {{ if eq .SequenceInfo.DurationHours 1 }} Hour {{ else }} Hours {{ end }}
            {{ end }}
            {{ if gt .SequenceInfo.DurationMin 0 }}
            {{ .SequenceInfo.DurationMin }}
            {{ if eq .SequenceInfo.DurationMin 1 }}
                Minute
            {{ else }}
                Minutes
            {{ end }}
            {{ end }}
            {{ end }}
            </p>
        </div>
        <div class="form-group">
            <label><strong>{{T "Number of Test Cases" .}}</strong></label>
            <p class="text-muted">
            {{ len .Cases }}
            </p>
        </div>
    {{ end }}

    <div class="form-group">
        <label><strong>{{T "Labels" .}}</strong></label>
        <span class="editLabels">
        {{if .User}}
            <button type="button" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#modal-manage-labels" >
                <i class="fa fa-wrench" aria-hidden="true"></i>
            </button>
            <div class="form-group">
                <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                    <strong>{{T "Assign" .}}</strong>
                </div>
                <div class="col-10 col-sm-11 col-md-12 col-lg-12" style="margin-top:0.5rem">
                    <div class="" style="overflow-y: scroll; max-height: 212px;border: 1px solid #ced4da;border-radius: .25rem;">
                        <div id="assignLabelContainer" class="col-12">
                        {{range .Project.Labels}}
                            <div id="assign-container-item-{{ .Id }}"
                                 class="input-group mb-3"
                                 onclick="onShowLabelClick({{ .Id }})"
                                 style="margin-top: 1rem;margin-bottom: 0rem;cursor: pointer;">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <i id="assign-container-icon-{{ .Id }}"
                                               class="fa input-group-text"
                                               data-toggle="tooltip"
                                               data-original-title="Click to assign/unassign"
                                               style="padding: 0.9rem"></i>
                                        </div>
                                        <span id="assign-container-label-{{ .Id }}"
                                              class="badge badge-primary clickIcon"
                                              data-toggle="tooltip"
                                              data-original-title="{{ .Description }}"
                                              style="background-color: {{ .Color }};
                                                      color: {{.TextColor}};
                                                      line-height: 2">{{ .Name }}</span>
                                    </div>
                                </form>
                            </div>
                        {{end}}
                        </div>
                    </div>
                </div>
            </div>
        {{end}}
        </span>

        <p id="showLabelContainer">
        {{ range .Project.Labels }}
            <span   id="show-container-label-{{ .Id }}"
                    class="badge badge-primary clickIcon"
                    data-toggle="tooltip"
                    data-original-title="{{ .Description }}"
                    style="background-color: {{ .Color }};
                            color: {{.TextColor}};
                            display: none">{{ .Name }}</span>
        {{ end }}
            <span id="showContainerText" class="text-muted">No Labels</span>
        </p>
    </div>

    <div class="form-group">
        <label><strong>{{T "Assignees" .}}</strong></label>
        {{ if and .ProjectPermissions.AssignSequence (eq .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr) }}
            <button class="btn btn-primary mr-2 float-right btn-sm" id="buttonAssign" data-toggle="modal"
                    data-target="#modal-tester-assignment">
                <i class="fa fa-user-plus" aria-hidden="true"></i>
            </button>
        {{ end }}
        <div id="testerContainer">
             {{if .Tasks }}
              <ul class="list-group">
                {{ range .Tasks }}
                    <li class="list-group-item">
                        <a href="/users/{{.Assignee}}"><img src="{{getImagePath (getUserImageFromID .Assignee) "user"}}" alt="" class="rounded-circle profile-picture" height="24" width="24"></a>
                        <a href="/users/{{.Assignee}}"><span class="text-muted assignmentAssignee">{{ .Assignee }}</span></a>
                        <span class="text-muted assignmentVersion">{{ .Version }}</span>
                        <span class="text-muted assignmentVariant">{{ .Variant }}</span>
                    </li>
                 </li>
                {{ end }}
                </ul>
            {{ else }}
                <ul>
                </ul>
                <span class="text-muted">{{T "No assigned testers" .}}</span>
            {{ end }}
        </div>
    </div>
    </div>
    </div>
</div>

{{template "modal-manage-labels" . }}


<!-- Import Scripts here -->
<script src="/static/js/project/sut-versions-show.js" integrity="{{sha256 "/static/js/project/sut-versions-show.js"}}"></script>
<script src="/static/js/util/common.js" integrity="{{sha256 "/static/js/util/common.js"}}"></script>
<script src="/static/js/project/testcases.js" integrity="{{sha256 "/static/js/project/testcases.js"}}"></script>
<script src="/static/js/project/testsequences.js" integrity="{{sha256 "/static/js/project/testsequences.js"}}"></script>
<script src="/static/assets/js/vendor/jquery.validate.min.js" integrity="{{sha256 "/static/assets/js/vendor/jquery.validate.min.js"}}"></script>

<!-- Scripts needed for labels -->
<script>
    testAssignedLabelIds = [
    {{range .TestSequence.Labels }}
        "{{.Id}}",
    {{end}}
    ];

    checkmarkAssignedLabels();
    showAssignedLabels();

    // Modal mode controls how the manage label modal interacts with its parent. 1 = show view
    modalMode = 1;

    setShowLabelText();
    setShowLabelTextForAssignments();
</script>


<script type='text/javascript'>
    $("#printerIcon").removeClass("d-none");
    $("#helpIcon").removeClass("d-none");

    // Listen to changes in the drop down menu of the variants
    setVersionOnChangeListener('#inputTestCaseSUTVersions', '#inputTestCaseSUTVariants');


    // Send a request to get the sut variants and versions
    var url = getTestURL().appendSegment("json").toString();

    // Request test case to get the sut versions from it
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            testObjectVersionData = {{ .SequenceInfo.Versions }}
            if (testObjectVersionData != null) {
                // Fill the drop down menu with the variants
                fillVersions(testObjectVersionData, "#inputTestCaseSUTVersions");
                // Update the version list with the versions of the selected variant
                updateVariantShowList('#inputTestCaseSUTVersions', '#inputTestCaseSUTVariants');
            }
        }
    };

    $("#buttonRevertConfirmed").click(revertTestSequence);

    // Reverts to selected older version of a testsequence.
    function revertTestSequence() {
        $('#revertModal').on('hidden.bs.modal', revertAjaxRequest).modal('hide');
    }

    $('#buttonDuplicateConfirmed').off('click').click(function () {

        $('#duplicateModal').off('click').one('hidden.bs.modal', function (e) {
            e.preventDefault();

            var url = currentURL().appendSegment("duplicate").toString() + "?fragment=true";

            var posting = $.post(url, {
                inputTestSequenceName: $('#inputDuplicateTestSequenceName').val(),
                version: "{{ .TestSequenceVersion.VersionNr }}"
            });

            posting.done(function (response) {
                var url = currentURL()
                        .takeFirstSegments(4)
                        .appendCodedSegment(posting.getResponseHeader("newName"))
                        .toString();

                $('#tabTestsequences').empty().append(response);
                history.pushState('data', '', url);
            }).fail(function (response) {
                $("#modalPlaceholder").empty().append(response.responseText);
                $('#errorModal').modal('show');
            });
        }).modal('hide');
    });

    function revertAjaxRequest(e) {
        e.preventDefault();

        /* Send the data using post with element ids*/
        var posting = $.ajax({
            url: currentURL().appendSegment("update").toString() + "?fragment=true",
            type: "PUT",
            data: getRevertData()
        });

        /* Alerts the results */
        posting.done(function (response) {
            var url = currentURL()
                    .takeFirstSegments(4)
                    .appendCodedSegment(posting.getResponseHeader("newName"))
                    .toString();

            $('#tabTestsequences').empty().append(response);
            history.pushState('data', '', url);
        }).fail(function (response) {
            $("#modalPlaceholder").empty().append(response.responseText);
            $('#errorModal').modal('show');
        });
    }

    function getRevertData() {
        return {
            //{{ with .TestSequenceVersion }}
            version: "{{ .VersionNr }}",
            inputCommitMessage: "Revert test case to version {{ .VersionNr }}",
            inputTestSequenceDescription: "{{ .Description }}",
            inputTestSequencePreconditions: "{{ .Preconditions }}",
            //{{ end }}
            //{{ with .TestSequence }}
            inputTestSequenceName: "{{ .Name }}"
            //{{ end }}
        }
    }

    $("#printerIcon").removeClass("d-none");

    function printer() {
        window.open(currentURL().appendSegment('print').toString(),
                'newwindow',
                'width=600,height=800');
        return false;
    }
</script>

<script type="text/javascript">
    assignButtonsTestSequence();

    $(".testCaseLine").click(showTestCaseVersionFromTestSequence);
</script>
{{end}}
