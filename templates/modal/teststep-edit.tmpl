{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "modal-teststep-edit"}}
<div class="modal fade" id="modal-teststep-edit" tabindex="-1" role="dialog" aria-labelledby="modal-teststep-edit-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-testcase-selection-label"><i class="fa fa-list-alt" aria-hidden="true"></i>
                    Test step</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="inputTestStepActionEditField"><strong>Task</strong></label>
                    <div class="md-area">
                        <textarea class="form-control markdown-textarea" id="inputTestStepActionEditField" rows="4"
                                  placeholder="Describe the task for this test step"></textarea>
                        <div class="md-area-bottom-toolbar">
                            <div style="float:left">{{T "Markdown supported" .}}</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputTestStepExpectedResultEditField"><strong>Expected Result</strong></label>
                    <div class="md-area">
                        <textarea class="form-control markdown-textarea" id="inputTestStepExpectedResultEditField" rows="4"
                                  placeholder="Enter the expected result"></textarea>
                        <div class="md-area-bottom-toolbar">
                            <div style="float:left">{{T "Markdown supported" .}}</div>
                        </div>
                    </div>
                </div>
                <div class="form-group d-none">
                    <input class="form-control invisible muted-text" id="testStepIdField">
                    <span id="add-1-edit-2"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button id="buttonFinishTestStepEditing" type="button" class="btn btn-primary">{{T "OK" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>

    <script type='text/javascript'>


        /* attach a handler to the add test cases form submit button */
        $("#buttonFinishTestStep").click(function(event) {
            var task = $("#inputTestStepActionEditField").val();
            var result = $("#inputTestStepExpectedResultEditField").val();

            if(task.trim()) {
                $('#modal-testcase-selection').modal('hide');
            }

        });

        $(document).on('click', '.remove-test-case', function() {
            if($(this).parent().parent().children().length < 2) {
                $(this).parent().parent().append("<li class=\"list-group-item\" id=\"testCaseListPlaceholder\"><span class=\"text-muted\">No Test Cases</span><button type=\"button\" class=\"add-test-case close ml-auto\" aria-label=\"Add Test Case\" data-toggle=\"modal\" data-target=\"#modal-testcase-selection\"><span aria-hidden=\"true\">+</span></button></li>");
            }
            $(this).parent().remove();
        });

    </script>
</div>
{{end}}