// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

func saveSUTVersionsForCaseVersion(s xorm.Interface, prID int64, cvrID int64, svs map[string]*project.Version) error {
	for _, sv := range svs {
		svrID, err := lookupSUTVersionRowID(s, prID, sv.Name)
		if err != nil {
			return err
		}

		err = saveSUTVariantsForCaseVersion(s, cvrID, svrID, sv.Variants)
		if err != nil {
			return err
		}
	}

	return nil
}

func listSUTVersionsForCaseVersion(s xorm.Interface, vrID int64) (map[string]*project.Version, error) {
	svrs, err := listSUTVariantRowsForCaseVersion(s, vrID)
	if err != nil {
		return nil, err
	}

	vs := make(map[int64][]project.Variant)
	for _, svr := range svrs {
		v := vs[svr.VersionID]
		sv := sutVariantFromRow(svr)
		v = append(v, sv)
		vs[svr.VersionID] = v
	}

	vars := make(map[string]*project.Version)
	for id, v := range vs {
		svr, ex, err := getSUTVersionRowByID(s, id)
		if err != nil {
			return nil, err
		}

		if ex {
			version := new(project.Version)
			version.Name = svr.Name
			version.Variants = v
			vars[svr.Name] = version
		}
	}

	return vars, nil
}

func getSUTVersionRowByID(s xorm.Interface, id int64) (sutVersionRow, bool, error) {
	var vr sutVersionRow
	ex, err := s.Table(sutVersionTable).ID(id).Get(&vr)
	if err != nil || !ex {
		return sutVersionRow{}, false, err
	}

	return vr, true, nil
}
