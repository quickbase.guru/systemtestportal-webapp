// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package store

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

func TestDeleteCase(t *testing.T) {
	engine = engineWithSampleData(t)
	cases := CasesSQL{engine}

	tc := test.Case{
		Name:    "test",
		Project: id.NewProjectID(id.NewActorID("default"), "DuckDuckGo.com"),
	}

	err := cases.Add(&tc)
	if err != nil {
		t.Errorf("Unexpected error while adding test case prior to removing:\n%s", err.Error())
	}

	err = cases.Delete(&tc)
	if err != nil {
		t.Errorf("Unexpected error while deleting test caes:\n%s", err.Error())
	}
}
