/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package middleware

import (
	"fmt"
	"net/http"

	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

// testSequenceKey we use our own type to avoid collision with other keys.
type testSequenceKey string

// TestSequenceKey can be used to retrieve a testsequence from a request context.
const TestSequenceKey = testSequenceKey("testsequence")

// testSequenceParamKey is used to retrieve the id from the request url parameters
const testSequenceParamKey = "testsequence"

const (
	nonExistentTestSequenceTitle = "The sequence you requested doesn't exist."
	nonExistentTestSequence      = "It seems that you requested a test sequence that doesn't exist anymore."
)

// TestSequenceStore provides an interface for retrieving sequence
type TestSequenceStore interface {

	// Get returns the test case with the given ID for the given project under the given container
	Get(sequenceID id.TestID) (*test.Sequence, bool, error)
}

// TestSequence is a middleware that can retrieve a testsequence from a request
// it requires the project as well as the container middleware to work.
func TestSequence(store TestSequenceStore) negroni.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		tsName, err := getParam(r, testSequenceParamKey)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		pID, err := getProjectID(r.Context().Value(ProjectKey), r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		tc, err := getTestSequence(store, id.NewTestID(pID, tsName, false), r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		AddToContext(r, TestSequenceKey, tc)
		next(w, r)
	}
}

func getTestSequence(store TestSequenceStore, tsID id.TestID, r *http.Request) (*test.Sequence, error) {
	ts, ok, err := store.Get(tsID)
	if !ok {
		return nil, errors.
			ConstructStd(http.StatusNotFound, nonExistentTestSequenceTitle, nonExistentTestSequence, r).
			WithLogf("Client request non-existent testsequence %v.", tsID).
			WithStackTrace(1).
			Finish()
	} else if err != nil {
		return nil, internalError(
			fmt.Sprintf("Unable to load sequence with id: %v.", tsID),
			err,
			r,
		)
	}
	return ts, nil
}
