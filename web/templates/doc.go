/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Package templates is used to easily retrieve parsed templates without hassle.

LoadedTemplate

To easily load cached templates and put them together the
LoadedTemplate facility can be used.

To load templates just use the load function which returns a LoadedTemplate:

	templates.Load("header", "content")

Here "header" and "content" are the names of the loaded templates.
As you can see the file extension and the base path is omitted.
The loader figures the paths out on his own.

Loaded templates are cached and won't be reread from the disk as long as they
remain in the cache. That is until they are removed from it like that:

	templates.Uncache("header")

Or by clearing the whole cache:

	templates.ClearCache()

The the newly loaded templates can be further used. And modified by for
example adding functions:

	templates.Load("header", "content").Func(template.FuncMap{"test" : Test})

Here "test" is the name of the function with which it is referred to inside the
template and Test is the function itself.

Functions used in a template need to be added before a template is loaded.
If you don't have loaded templates before hand you can just use:

	template.Load().Func(template.FuncMap{"test" : Test}).Append("usesTest")

An empty load clause given you a LoadedTemplate so you can add functions before
actually loading a template.

Here you can already see the Append function of a LoadedTemplate which can be
used to append new templates to the bunch later down the road.

Finally if you want to execute a specific template you can use Lookup find it.
Lets say the content template contains a definition of the "tab" template
and we want to get it:

	template.Load("header", "content").Lookup("tab")

But be careful other than the other methods the Lookup function doesn't return
the LoadedTemplate it is used on. It actually returns only the "tab" template
which doesn't include the templates in "header" or "content", but only the
"tab" template and every template defined inside it.

After you have found the template you want to execute you can use the Get function
to retrieve the actual template from the standard library and execute it:

	template.Load("header", "content").Lookup("tab").Get().Execute(w, c)

This will load the "header" and "content" template files and retrieve the "tab"
template out of them and execute it on the writer w with the context c.

*/
package templates
