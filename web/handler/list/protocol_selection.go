/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"net/http"
)

// Selection contains which test is selected
type Selection struct {
	SelectedCase,
	SelectedSequence,
	SelectedType string
}

// GetSelection gets the selected test from the request
// and returns a selection item.
// If there is no selection in the parameter, the selection
// is set to "show all test cases"
func GetSelection(r *http.Request) Selection {

	cs, err := r.Cookie("selected")
	ct, ers := r.Cookie("type")

	var sel Selection

	if err == nil && ers == nil {
		if ct.Value == "testcases" || ct.Value == "testsequences" {
			sel = Selection{
				SelectedCase:     cs.Value,
				SelectedSequence: cs.Value,
				SelectedType:     ct.Value,
			}
			return sel
		}
	}

	sel.SelectedType = "testcases"
	sel.SelectedSequence = ""
	sel.SelectedCase = ""

	return sel
}
