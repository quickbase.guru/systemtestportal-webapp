/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"net/http"
	"reflect"
	"strconv"
	"strings"

	"regexp"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// testSequenceEditInput contains the general testSequenceInput
// plus edit information.
type testSequenceEditInput struct {
	creation.TestSequenceInput
	InputCommitMessage,
	IsMinor string
}

// SequencePut handles requests that demand to update a sequence
func SequencePut(t handler.TestCaseGetter, tsu handler.TestSequenceUpdater,
	sequenceChecker id.TestExistenceChecker, commentStore handler.Comments,
	taskGetter handler.TaskGetter, lister handler.TestSequenceLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).EditSequence {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		us, err := updateTestSequence(r, t, tsu, sequenceChecker, c.Sequence)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		c.Sequence = us

		w.Header().Set(httputil.NewName, c.Sequence.ID().Test())

		comments, err := commentStore.GetCommentsForTest(*c.Sequence, c.Project, c.User)
		if err != nil {
			errors.Handle(err, w, r)
		}

		testers, err := taskGetter.GetTasksForTest(c.Sequence.ID())

		w.WriteHeader(http.StatusCreated)
		r.Form.Set(httputil.Version, "")

		display.ShowSequence(c.Project, c.Sequence, comments, testers, lister, w, r)

	}
}

// getTestSequenceInput gets the test sequence input from the request
func getTestSequenceEditInput(r *http.Request) (testSequenceEditInput, error) {
	input, err := creation.GetTestSequenceInput(r, false)
	return testSequenceEditInput{
		*input,
		r.FormValue(httputil.CommitMessage),
		r.FormValue(httputil.IsMinor),
	}, err
}

// Error messages of updating a test sequence
const (
	errCanNotUpdateTestSequenceTitle = "Couldn't execute your edit as requested."
	errCanNotUpdateTestSequence      = "We were unable to apply the changes you requested for your " +
		"test sequence. If you believe this is a bug please contact us via our " + handler.IssueTracker + "."
	errCanNotRenameTestSequenceTitle = "Unable to rename test sequence"
	errCanNotRenameTestSequence      = "We were unable to successfully rename your test sequence. " +
		"If you believe this is a bug please contact us via our " + handler.IssueTracker + "."
)

// updateTestSequenceStore applies an update to given sequence (old) using the new version (new)
// This will access the storage and finally write the changes.
func updateTestSequenceStore(r *http.Request, t handler.TestSequenceUpdater, sequenceChecker id.TestExistenceChecker,
	new *test.Sequence, oldID *id.TestID) error {

	if new.ID() != *oldID {
		if err := t.Rename(*oldID, new.ID()); err != nil {
			return errors.ConstructStd(http.StatusInternalServerError,
				errCanNotRenameTestSequenceTitle, errCanNotRenameTestSequence, r).
				WithLogf("Couldn't rename test sequence %+v.", oldID).
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Finish()
		}
	}

	if err := t.Add(new); err != nil {
		return errors.ConstructStd(http.StatusInternalServerError,
			errCanNotUpdateTestSequenceTitle, errCanNotUpdateTestSequence, r).
			WithLogf("Couldn't update test sequence %+v.", new.ID()).
			WithStackTrace(1).
			WithCause(err).
			WithRequestDump(r).
			Finish()
	}

	return nil
}

const (
	errInvalidSequenceVersionTitle = "Test sequence without versions."
	errInvalidSequenceVersion      = "It seems your tried to edit a test sequence that " +
		"wasn't created correctly. This is most likely a bug. If you want please " +
		"contact us via our " + handler.IssueTracker + "."

	errInvalidIsMinorValueTitle = "Can't determine if edit is minor."
	errInvalidIsMinorValue      = "We couldn't determine whether your edit was minor or not " +
		"based on your request. This is most likely a bug. If you want please " +
		"contact us via our " + handler.IssueTracker + "."
)

// NoSequenceVersions returns an ErrorMessage describing that the test sequence doesn't have
// a version.
func NoSequenceVersions(r *http.Request, testID id.TestID) errors.HandlerError {
	return errors.ConstructStd(http.StatusBadRequest, errInvalidSequenceVersionTitle,
		errInvalidSequenceVersion, r).
		WithLogf("The client tried to edit a test sequence that has no versions.\n"+
			"The id was: %+v", testID).
		WithStackTrace(2).
		Finish()
}

// InvalidIsMinor returns an error describing that the client send an invalid
// value for isMinor.
func InvalidIsMinor(r *http.Request, v string, cause error) errors.HandlerError {
	return errors.ConstructStd(http.StatusBadRequest, errInvalidIsMinorValueTitle,
		errInvalidIsMinorValue, r).
		WithLogf("The client send invalid value for isMinor: %q.", v).
		WithStackTrace(2).
		WithCause(cause).
		Finish()
}

// createNewTestSequenceVersion will create a new testsequence using given testsequence as basis.
// A new version will be added using given input. If an error occurs is will be returned instead
// and the returned testsequence will be nil.
func createNewTestSequenceVersion(t handler.TestCaseGetter, ts test.Sequence,
	input testSequenceEditInput, r *http.Request) (*test.Sequence, error) {

	isMinor, err := strconv.ParseBool(input.IsMinor)
	if err != nil {
		return nil, InvalidIsMinor(r, input.IsMinor, err)
	}

	latest := len(ts.SequenceVersions)
	if latest <= 0 {
		return nil, NoSequenceVersions(r, ts.ID())
	}

	tcIDs := getSequenceIds(input.InputTestSequenceTestCase, ts)

	tsc, err := handler.GetTestCases(t, tcIDs...)
	if err != nil {
		return nil, err
	}

	tcv, err := test.NewTestSequenceVersion(latest+1, isMinor,
		input.InputCommitMessage,
		input.InputTestSequenceDescription,
		input.InputTestSequencePreconditions,
		tsc,
		ts.ID())
	if err != nil {
		return nil, err
	}

	// Insert at beginning of slice
	ts.SequenceVersions = append([]test.SequenceVersion{tcv}, ts.SequenceVersions...)
	return &ts, nil
}

func getSequenceIds(input string, ts test.Sequence) []id.TestID {
	if input == "" {
		return make([]id.TestID, 0)
	}
	tcNames := strings.Split(input, "/")
	tcIDs := make([]id.TestID, len(tcNames))
	for i, tcName := range tcNames {
		tcIDs[i] = id.NewTestID(ts.Project, tcName, true)
	}
	return tcIDs
}

// handleSequenceRename renames and validates the sequence and then updates the protocol store.
// The function returns the test case and an error if any occurred.
func handleSequenceRename(ts *test.Sequence, input testSequenceEditInput, sequenceChecker id.TestExistenceChecker,
	oldTSID id.TestID) (*test.Sequence, error) {

	ts.Rename(input.InputTestSequenceName)

	// Validate new name
	if err := ts.ID().Validate(sequenceChecker); err != nil {
		return nil, err
	}

	return ts, nil
}

// testSequenceMetaDataChanged checks whether the test sequence to update (input)
// has different metadata than the newest version of the existing
// test sequence (ts). Returns true if there are changes.
// Else return false.
func testSequenceMetaDataChanged(input *testSequenceEditInput, ts *test.Sequence, t handler.TestCaseGetter) bool {
	testSequenceVersion := ts.SequenceVersions[0]
	if input.InputTestSequenceDescription != testSequenceVersion.Description {
		return true
	}
	if !reflect.DeepEqual(input.InputTestSequencePreconditions, testSequenceVersion.Preconditions) {
		return true
	}
	if testCasesChanged(input.InputTestSequenceTestCase, &testSequenceVersion) {
		return true
	}
	return false
}

// updateTestSequence updates a test sequence using the input from the request.
// If only the test sequence name was changed, no  new version of a test sequence
// is created. Instead only the name is changed in the store.
// Returns the changed test sequence or an error if the input is not valid or
// the test sequence could not be updated.
func updateTestSequence(r *http.Request, t handler.TestCaseGetter, tsu handler.TestSequenceUpdater,
	sequenceChecker id.TestExistenceChecker,
	ts *test.Sequence) (*test.Sequence, error) {

	input, err := getTestSequenceEditInput(r)
	if err != nil {
		return nil, err
	}

	input.InputTestSequenceName = strings.TrimSpace(input.InputTestSequenceName)
	re := regexp.MustCompile(`\s\s+`)
	input.InputTestSequenceName = re.ReplaceAllString(input.InputTestSequenceName, " ")

	// Save the id of the old test sequence to delete the old entry from the store
	var oldTSID = id.NewTestID(ts.ID().ProjectID, ts.ID().Test(), ts.ID().IsCase())

	var nameChanged = ts.Name != input.InputTestSequenceName

	// If the name has changed -> Rename sequence and validate new name
	if nameChanged {
		ts, err = handleSequenceRename(ts, input, sequenceChecker, oldTSID)
		if err != nil {
			return nil, err
		}
	}

	var metaDataChanged = testSequenceMetaDataChanged(&input, ts, t)

	// Create a new version of the sequence if the metadata changed
	if metaDataChanged {
		nts, err := createNewTestSequenceVersion(t, *ts, input, r)
		if err != nil {
			return nil, err
		}
		ts = nts
	}

	// Update test sequence store
	if nameChanged || metaDataChanged {
		if err := updateTestSequenceStore(r, tsu, sequenceChecker, ts, &oldTSID); err != nil {
			return nil, err
		}
	}

	return ts, nil
}

// testCasesChanged checks whether the cases of the test sequence to update (testCasesString)
// have changed compared to the existing test case (ts)
// Returns true if they have changed. Return false if they didn't change or could not be
// parsed.
func testCasesChanged(testCasesString string, tsv *test.SequenceVersion) bool {

	// Get names of the new test casees
	tcNames := strings.Split(testCasesString, "~")

	// Check if the existing cases and the new ones are different
	for tsCaseIndex, tsCase := range tsv.Cases {
		if tsCase.Name != tcNames[tsCaseIndex] {
			return true
		}
	}
	return false
}
